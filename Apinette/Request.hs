{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE UndecidableInstances #-}

module Apinette.Request
  ( Request (..),
    Method (..),
    Url (..),
    Auth (..),
    BasicAuth (..),
    BearerAuth (..),
    ContentType (..),
    Body (..),
    Headers (..),
    CreateRequest (..),
    ModifyRequest (..),
  )
where

import Control.Monad.Catch (MonadThrow (..))
import Control.Monad.IO.Class (MonadIO (..))
import Data.Aeson (FromJSON (..), ToJSON (..))
import qualified Data.Aeson as Aeson
import qualified Data.CaseInsensitive as CaseInsensitive
import Data.Default (Default (..))
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Encoding as Text
import GHC.Generics
import qualified Network.HTTP.Client as HTTP
import qualified Network.HTTP.Types as HTTP

class (MonadThrow m, MonadIO m) => CreateRequest m req where
  createRequest :: req -> m HTTP.Request

class (MonadThrow m, MonadIO m) => ModifyRequest m req where
  modifyRequest :: req -> HTTP.Request -> m HTTP.Request

instance (MonadThrow m, MonadIO m, CreateRequest m req) => CreateRequest m (Maybe req) where
  createRequest = \case
    Nothing -> return HTTP.defaultRequest
    Just req -> createRequest req

instance (MonadThrow m, MonadIO m, ModifyRequest m req) => ModifyRequest m (Maybe req) where
  modifyRequest = \case
    Nothing -> return
    Just req -> modifyRequest req

data Request
  = Request
      { method :: Maybe Method,
        url :: Maybe Url,
        auth :: Maybe Auth,
        contentType :: Maybe ContentType,
        body :: Maybe Body,
        headers :: Maybe Headers
      }
  deriving (Generic)

customOptions :: Aeson.Options
customOptions = Aeson.defaultOptions {Aeson.unwrapUnaryRecords = True}

instance Default Request

instance FromJSON Request where
  parseJSON = Aeson.genericParseJSON customOptions

instance ToJSON Request where

  toJSON = Aeson.genericToJSON customOptions

  toEncoding = Aeson.genericToEncoding customOptions

instance (MonadThrow m, MonadIO m) => CreateRequest m Request where
  createRequest Request {..} = do
    createRequest url
      >>= modifyRequest method
      >>= modifyRequest auth
      >>= modifyRequest contentType
      >>= modifyRequest body
      >>= modifyRequest headers

newtype Method
  = Method {methodText :: Text}
  deriving (Generic)

instance FromJSON Method where
  parseJSON = Aeson.genericParseJSON customOptions

instance ToJSON Method where

  toJSON = Aeson.genericToJSON customOptions

  toEncoding = Aeson.genericToEncoding customOptions

instance (MonadThrow m, MonadIO m) => ModifyRequest m Method where
  modifyRequest Method {..} req =
    return $ req {HTTP.method = Text.encodeUtf8 methodText}

newtype Url
  = Url {urlText :: Text}
  deriving (Generic)

instance FromJSON Url where
  parseJSON = Aeson.genericParseJSON customOptions

instance ToJSON Url where

  toJSON = Aeson.genericToJSON customOptions

  toEncoding = Aeson.genericToEncoding customOptions

instance (MonadThrow m, MonadIO m) => CreateRequest m Url where
  createRequest Url {..} =
    HTTP.parseRequest url
    where
      url = Text.unpack urlText

data Auth
  = Basic BasicAuth
  | Bearer BearerAuth
  deriving (Generic)

instance FromJSON Auth where
  parseJSON = Aeson.genericParseJSON customOptions

instance ToJSON Auth where

  toJSON = Aeson.genericToJSON customOptions

  toEncoding = Aeson.genericToEncoding customOptions

instance (MonadThrow m, MonadIO m) => ModifyRequest m Auth where
  modifyRequest = \case
    Basic basicAuth -> modifyRequest basicAuth
    Bearer bearerAuth -> modifyRequest bearerAuth

data BasicAuth
  = BasicAuth
      { user :: Text,
        pass :: Text
      }
  deriving (Generic)

instance FromJSON BasicAuth where
  parseJSON = Aeson.genericParseJSON customOptions

instance ToJSON BasicAuth where

  toJSON = Aeson.genericToJSON customOptions

  toEncoding = Aeson.genericToEncoding customOptions

instance (MonadThrow m, MonadIO m) => ModifyRequest m BasicAuth where
  modifyRequest BasicAuth {..} req =
    return $ HTTP.applyBasicAuth user' pass' req
    where
      user' = Text.encodeUtf8 user
      pass' = Text.encodeUtf8 pass

data BearerAuth
  = BearerAuth
      { token :: Text
      }
  deriving (Generic)

instance FromJSON BearerAuth where
  parseJSON = Aeson.genericParseJSON customOptions

instance ToJSON BearerAuth where

  toJSON = Aeson.genericToJSON customOptions

  toEncoding = Aeson.genericToEncoding customOptions

instance (MonadThrow m, MonadIO m) => ModifyRequest m BearerAuth where
  modifyRequest BearerAuth {..} req =
    return $ req {HTTP.requestHeaders = auth : HTTP.requestHeaders req}
    where
      token' = Text.encodeUtf8 token
      auth = (HTTP.hAuthorization, "Bearer " <> token')

newtype ContentType
  = ContentType {contentTypeText :: Text}
  deriving (Generic)

instance FromJSON ContentType where
  parseJSON = Aeson.genericParseJSON customOptions

instance ToJSON ContentType where

  toJSON = Aeson.genericToJSON customOptions

  toEncoding = Aeson.genericToEncoding customOptions

instance (MonadThrow m, MonadIO m) => ModifyRequest m ContentType where
  modifyRequest ContentType {..} req =
    return $ req {HTTP.requestHeaders = contentType : HTTP.requestHeaders req}
    where
      contentType = (HTTP.hContentType, Text.encodeUtf8 contentTypeText)

newtype Body
  = Body {bodyText :: Text}
  deriving (Generic)

instance FromJSON Body where
  parseJSON = Aeson.genericParseJSON customOptions

instance ToJSON Body where

  toJSON = Aeson.genericToJSON customOptions

  toEncoding = Aeson.genericToEncoding customOptions

instance (MonadThrow m, MonadIO m) => ModifyRequest m Body where
  modifyRequest Body {..} req =
    return $ req {HTTP.requestBody = HTTP.RequestBodyBS body}
    where
      body = Text.encodeUtf8 bodyText

newtype Headers
  = Headers {headersList :: [(Text, Text)]}
  deriving (Generic)

instance FromJSON Headers where
  parseJSON = Aeson.genericParseJSON customOptions

instance ToJSON Headers where

  toJSON = Aeson.genericToJSON customOptions

  toEncoding = Aeson.genericToEncoding customOptions

instance (MonadThrow m, MonadIO m) => ModifyRequest m Headers where
  modifyRequest Headers {..} req =
    return $ req {HTTP.requestHeaders = headers}
    where
      headers =
        (\(h, v) -> (CaseInsensitive.mk $ Text.encodeUtf8 h, Text.encodeUtf8 v))
          <$> headersList
