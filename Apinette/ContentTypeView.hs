{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

module Apinette.ContentTypeView
  ( ContentTypeView,
    contentTypeCombo,
  )
where

import qualified Apinette.Request as Val
import Apinette.Value (IsValue (..))
import Apinette.View (IsView (..))
import Control.Monad.IO.Class (MonadIO (..))
import Data.GI.Base
import qualified Data.Text as Text
import qualified GI.Gtk as Gtk

data ContentTypeView
  = ContentTypeView
      { box :: !Gtk.Box,
        contentTypeCombo :: !Gtk.ComboBoxText
      }

instance (MonadIO m) => IsView m ContentTypeView where
  type Props ContentTypeView = ()

  create () = do
    box <-
      new
        Gtk.Box
        [ #orientation := Gtk.OrientationHorizontal,
          #spacing := 1
        ]
    contentTypeCombo <- Gtk.comboBoxTextNewWithEntry
    mapM_
      (Gtk.comboBoxTextAppendText contentTypeCombo)
      [ "",
        "application/json",
        "application/xml",
        "text/plain",
        "text/html"
      ]
    set contentTypeCombo [#active := 0]
    #add box contentTypeCombo
    #showAll box
    return ContentTypeView {..}

  widget = Gtk.toWidget . box

instance (MonadIO m) => IsValue m ContentTypeView where
  type Value ContentTypeView = Maybe Val.ContentType

  toValue ContentTypeView {..} = do
    Gtk.comboBoxTextGetActiveText contentTypeCombo >>= \case
      Nothing -> return Nothing
      Just x ->
        if Text.strip x == mempty
          then return Nothing
          else return . Just $ Val.ContentType x

  fromValue ContentTypeView {..} contentTypeMay = do
    let contentType = maybe "" Val.contentTypeText contentTypeMay
    widgetMay <- Gtk.binGetChild contentTypeCombo
    case widgetMay of
      Nothing ->
        return ()
      Just widget' -> do
        entryMay <- liftIO $ Gtk.castTo Gtk.Entry widget'
        case entryMay of
          Nothing ->
            return ()
          Just entry -> do
            set entry [#text := contentType]
