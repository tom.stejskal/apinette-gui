{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

module Apinette.RequestBodyView
  ( RequestBodyView,
  )
where

import qualified Apinette.Request as Val
import Apinette.Value (IsValue (..))
import Apinette.View (IsView (..))
import Control.Monad (void)
import Control.Monad.IO.Class (MonadIO (..))
import Data.Default (Default (..))
import Data.GI.Base
import qualified GI.Gtk as Gtk
import qualified GI.GtkSource as GtkSource

data RequestBodyView
  = RequestBodyView
      { box :: !Gtk.Box,
        contentTypeCombo :: !Gtk.ComboBoxText,
        bodyView :: !GtkSource.View,
        lm :: !GtkSource.LanguageManager
      }

instance (MonadIO m) => IsView m RequestBodyView where
  type Props RequestBodyView = GtkSource.LanguageManager

  create lm = do
    box <-
      new
        Gtk.Box
        [ #orientation := Gtk.OrientationVertical,
          #spacing := 10,
          #margin := 10
        ]
    contentTypeBox <-
      new
        Gtk.Box
        [ #orientation := Gtk.OrientationHorizontal,
          #spacing := 10
        ]
    contentTypeCombo <- Gtk.comboBoxTextNewWithEntry
    mapM_
      (Gtk.comboBoxTextAppendText contentTypeCombo)
      [ "application/json",
        "application/xml",
        "text/plain",
        "text/html"
      ]
    set contentTypeCombo [#active := 0]
    #add contentTypeBox contentTypeCombo
    #add box contentTypeBox
    bodyFrame <- new Gtk.Frame []
    scrollWin <- new Gtk.ScrolledWindow [#margin := 10]
    #add bodyFrame scrollWin
    #add box bodyFrame
    bodyView <-
      new
        GtkSource.View
        [ #hexpand := True,
          #vexpand := True
        ]
    #add scrollWin bodyView
    #showAll box
    let result = RequestBodyView {..}
    void $ after contentTypeCombo #changed $ contentTypeChanged result
    return result

  widget = Gtk.toWidget . box

instance (MonadIO m) => IsValue m RequestBodyView where
  type Value RequestBodyView = Val.Request

  toValue RequestBodyView {..} = do
    contentType <- Gtk.comboBoxTextGetActiveText contentTypeCombo
    buffer <- get bodyView #buffer
    body <- get buffer #text
    return $
      def
        { Val.contentType = Val.ContentType <$> contentType,
          Val.body = Val.Body <$> body
        }

  fromValue requestBodyView@RequestBodyView {..} Val.Request {..} = do
    case contentType of
      Nothing ->
        return ()
      Just contentType' -> do
        widgetMay <- Gtk.binGetChild contentTypeCombo
        case widgetMay of
          Nothing ->
            return ()
          Just widget' -> do
            entryMay <- liftIO $ Gtk.castTo Gtk.Entry widget'
            case entryMay of
              Nothing ->
                return ()
              Just entry -> do
                set entry [#text := Val.contentTypeText contentType']
                contentTypeChanged requestBodyView
    buffer <- get bodyView #buffer
    set buffer [#text := maybe "" Val.bodyText body]

contentTypeChanged :: (MonadIO m) => RequestBodyView -> m ()
contentTypeChanged RequestBodyView {..} = do
  contentType <- Gtk.comboBoxTextGetActiveText contentTypeCombo
  lang <-
    case contentType of
      Nothing -> return Nothing
      Just "" -> return Nothing
      Just x -> GtkSource.languageManagerGuessLanguage lm Nothing $ Just x
  buffer <- liftIO $ Gtk.castTo GtkSource.Buffer =<< get bodyView #buffer
  case buffer of
    Just buffer' ->
      GtkSource.bufferSetLanguage buffer' lang
    _ ->
      return ()
