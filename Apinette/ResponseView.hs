{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}

module Apinette.ResponseView
  ( ResponseView,
    setStatus,
  )
where

import Apinette.Response (FromResponse (..))
import Apinette.View (IsView (..))
import Control.Monad (void)
import Control.Monad.IO.Class (MonadIO (..))
import qualified Data.Aeson as Aeson
import qualified Data.Aeson.Encode.Pretty as Aeson
import qualified Data.ByteString.Lazy as Lazy
import Data.GI.Base
import qualified Data.Maybe as Maybe
import qualified Data.Text as Text
import Data.Text (Text)
import qualified Data.Text.Encoding as Text
import qualified GI.Gtk as Gtk
import qualified GI.GtkSource as GtkSource
import qualified Network.HTTP.Client as HTTP
import qualified Network.HTTP.Types as HTTP

data ResponseView
  = ResponseView
      { box :: !Gtk.Box,
        statusLabel :: !Gtk.Label,
        bodyView :: !GtkSource.View,
        lm :: !GtkSource.LanguageManager
      }

instance (MonadIO m) => IsView m ResponseView where

  type Props ResponseView = GtkSource.LanguageManager

  create lm = do
    box <-
      new
        Gtk.Box
        [ #orientation := Gtk.OrientationVertical,
          #spacing := 10
        ]
    statusBox <-
      new
        Gtk.Box
        [ #orientation := Gtk.OrientationHorizontal,
          #spacing := 10
        ]
    statusLabel <- new Gtk.Label [#label := ""]
    #add statusBox statusLabel
    #add box statusBox
    bodyNotebook <- new Gtk.Notebook [#heightRequest := 200]
    bodyScroll <- new Gtk.ScrolledWindow []
    bodyView <-
      new
        GtkSource.View
        [ #hexpand := True,
          #vexpand := True,
          #editable := False
        ]
    #add bodyScroll bodyView
    bodyLabel <- new Gtk.Label [#label := "Response"]
    void $ Gtk.notebookAppendPage bodyNotebook bodyScroll $ Just bodyLabel
    #add box bodyNotebook
    #showAll box
    let result = ResponseView {..}
    setStatus result ""
    return result

  widget = Gtk.toWidget . box

instance (MonadIO m) => FromResponse m Lazy.ByteString ResponseView where
  fromResponse ResponseView {..} res time = do
    set
      statusLabel
      [ #label := "Status: "
          <> (Text.pack . show . HTTP.statusCode $ status)
          <> " "
          <> (Text.decodeUtf8 . HTTP.statusMessage $ status)
          <> " ("
          <> (Text.pack . show $ time)
          <> ")"
      ]
    buffer <- get bodyView #buffer
    text <- decodeBody
    set buffer [#text := text]
    where
      status = HTTP.responseStatus res
      headers = HTTP.responseHeaders res
      contentType =
        snd <$> (Maybe.listToMaybe $ filter (\(h, _) -> h == HTTP.hContentType) headers)
      decodeBody :: (MonadIO m) => m Text
      decodeBody = do
        case contentType of
          Just contentType' -> do
            lang <-
              GtkSource.languageManagerGuessLanguage
                lm
                Nothing
                (Just $ Text.decodeUtf8 contentType')
            buffer <- liftIO $ Gtk.castTo GtkSource.Buffer =<< get bodyView #buffer
            case buffer of
              Just buffer' ->
                GtkSource.bufferSetLanguage buffer' lang
              _ ->
                return ()
            if contentType' == "application/json"
              then
                return $ maybe "" id $
                  Text.decodeUtf8 . Lazy.toStrict . Aeson.encodePretty
                    <$> (Aeson.decode @Aeson.Value (HTTP.responseBody res))
              else return $ Text.decodeUtf8 . Lazy.toStrict $ HTTP.responseBody res
          _ ->
            return $ Text.decodeUtf8 . Lazy.toStrict $ HTTP.responseBody res

setStatus :: (MonadIO m) => ResponseView -> Text -> m ()
setStatus ResponseView {..} text =
  set statusLabel [#label := "Status: " <> text]
