{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}

module Apinette.RequestView
  ( RequestView,
    open,
    save,
    saveAs,
    sendRequest,
  )
where

import Apinette.AuthView (AuthView)
import Apinette.ContentTypeView (ContentTypeView, contentTypeCombo)
import qualified Apinette.Request as Val
import Apinette.Request (CreateRequest (..))
import Apinette.Response (FromResponse (..))
import Apinette.ResponseView (ResponseView, setStatus)
import Apinette.UrlView (UrlView, sendButton)
import Apinette.Value (IsValue (..))
import Apinette.View (IsView (..))
import Control.Concurrent.Async (async)
import Control.Exception (IOException, try)
import Control.Monad (void)
import Control.Monad.Catch (MonadThrow (..))
import Control.Monad.IO.Class (MonadIO (..))
import qualified Data.Aeson as Aeson
import qualified Data.ByteString.Lazy as ByteString
import Data.Default (Default (..))
import Data.GI.Base
import Data.IORef (IORef, newIORef, readIORef, writeIORef)
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Time.Clock as Clock
import qualified GI.GLib as GLib
import qualified GI.Gtk as Gtk
import qualified GI.GtkSource as GtkSource
import qualified Network.HTTP.Client as HTTP
import qualified Network.HTTP.Client.TLS as HTTPS

data RequestView
  = RequestView
      { box :: !Gtk.Box,
        appWin :: !Gtk.Window,
        appName :: !Text,
        urlView :: !UrlView,
        authView :: !AuthView,
        contentTypeView :: !ContentTypeView,
        paned :: !Gtk.Paned,
        bodyView :: !GtkSource.View,
        resView :: !ResponseView,
        lm :: !GtkSource.LanguageManager,
        fileName :: !(IORef (Maybe String))
      }

instance (MonadThrow m, MonadIO m) => IsView m RequestView where
  type Props RequestView = (Gtk.Window, Text)

  create (appWin, appName) = do
    lm <- new GtkSource.LanguageManager []
    box <-
      new
        Gtk.Box
        [ #orientation := Gtk.OrientationVertical,
          #margin := 10,
          #spacing := 10,
          #hexpand := True,
          #vexpand := True
        ]
    urlView <- create ()
    #add box =<< widget urlView
    authView <- create ()
    #add box =<< widget authView
    contentTypeView <- create ()
    #add box =<< widget contentTypeView
    paned <- new Gtk.Paned [#orientation := Gtk.OrientationVertical]
    bodyNotebook <-
      new
        Gtk.Notebook
        [ #marginBottom := 10,
          #heightRequest := 200
        ]
    bodyScroll <- new Gtk.ScrolledWindow []
    bodyView <-
      new
        GtkSource.View
        [ #hexpand := True,
          #vexpand := True,
          #margin := 10
        ]
    #add bodyScroll bodyView
    bodyLabel <- new Gtk.Label [#label := "Body"]
    void $ Gtk.notebookAppendPage bodyNotebook bodyScroll $ Just bodyLabel
    #add paned bodyNotebook
    resView <- create lm
    resWidget <- widget resView
    set resWidget [#marginTop := 10]
    #add paned resWidget
    #add box paned
    #showAll box
    fileName <- liftIO $ newIORef Nothing
    let result = RequestView {..}
    let send = sendButton urlView
    void $ on send #clicked $ sendRequest result
    void $ after (contentTypeCombo contentTypeView) #changed $ contentTypeChanged result
    contentTypeChanged result
    return result

  widget = Gtk.toWidget . box

instance (MonadIO m) => IsValue m RequestView where
  type Value RequestView = Val.Request

  toValue RequestView {..} = do
    (method, url) <- toValue urlView
    auth <- toValue authView
    contentType <- toValue contentTypeView
    buffer <- get bodyView #buffer
    body <- get buffer #text
    return $
      def
        { Val.method = method,
          Val.url = url,
          Val.auth = auth,
          Val.contentType = contentType,
          Val.body = Val.Body <$> body
        }

  fromValue requestView@RequestView {..} req@Val.Request {..} = do
    fromValue urlView (Val.method req, Val.url req)
    fromValue authView auth
    fromValue contentTypeView $ Val.contentType req
    buffer <- get bodyView #buffer
    set buffer [#text := maybe "" Val.bodyText body]
    contentTypeChanged requestView

instance (MonadThrow m, MonadIO m) => CreateRequest m RequestView where
  createRequest requestView =
    toValue requestView >>= createRequest

setFileName :: (MonadIO m) => RequestView -> Maybe String -> m ()
setFileName RequestView {..} fileNameMay = do
  liftIO $ writeIORef fileName fileNameMay
  set
    appWin
    [ #title
        := maybe
          appName
          ( \fileName' ->
              appName <> " - " <> Text.pack fileName'
          )
          fileNameMay
    ]

saveAs :: (MonadIO m) => RequestView -> m ()
saveAs requestView@RequestView {..} = do
  dlg <-
    new
      Gtk.FileChooserDialog
      [ #transientFor := appWin,
        #modal := True,
        #title := "Save file",
        #action := Gtk.FileChooserActionSave,
        #doOverwriteConfirmation := True
      ]
  void $ Gtk.dialogAddButton dlg "Cancel" $ fromIntegral $
    fromEnum Gtk.ResponseTypeCancel
  void $ Gtk.dialogAddButton dlg "Save" $ fromIntegral $
    fromEnum Gtk.ResponseTypeAccept
  fileNameMay <- liftIO $ readIORef fileName
  case fileNameMay of
    Nothing ->
      void $ Gtk.fileChooserSetCurrentName dlg "New file"
    Just fileName' ->
      void $ Gtk.fileChooserSetFilename dlg fileName'
  void $ on dlg #response $ onResponse dlg
  void $ Gtk.dialogRun dlg
  where
    onResponse dlg res = do
      if res == fromIntegral (fromEnum Gtk.ResponseTypeAccept)
        then do
          fileNameMay <- Gtk.fileChooserGetFilename dlg
          setFileName requestView fileNameMay
          case fileNameMay of
            Nothing ->
              return ()
            Just fileName' -> do
              saveToFile requestView fileName'
        else return ()
      Gtk.widgetDestroy dlg

save :: (MonadIO m) => RequestView -> m ()
save requestView@RequestView {..} = do
  fileNameMay <- liftIO $ readIORef fileName
  case fileNameMay of
    Nothing ->
      saveAs requestView
    Just fileName' ->
      saveToFile requestView fileName'

saveToFile :: (MonadIO m) => RequestView -> String -> m ()
saveToFile requestView fileName' = do
  req <- toValue requestView
  liftIO $ Aeson.encodeFile fileName' req

open :: (MonadIO m) => RequestView -> m ()
open requestView@RequestView {..} = do
  dlg <-
    new
      Gtk.FileChooserDialog
      [ #transientFor := appWin,
        #modal := True,
        #title := "Open file",
        #action := Gtk.FileChooserActionOpen
      ]
  void $ Gtk.dialogAddButton dlg "Cancel" $ fromIntegral $
    fromEnum Gtk.ResponseTypeCancel
  void $ Gtk.dialogAddButton dlg "Load" $ fromIntegral $
    fromEnum Gtk.ResponseTypeAccept
  void $ on dlg #response $ onResponse dlg
  void $ Gtk.dialogRun dlg
  where
    onResponse dlg res = do
      if res == fromIntegral (fromEnum Gtk.ResponseTypeAccept)
        then do
          fileNameMay <- Gtk.fileChooserGetFilename dlg
          setFileName requestView fileNameMay
          case fileNameMay of
            Nothing ->
              return ()
            Just fileName' -> do
              (try @IOException $ liftIO $ ByteString.readFile fileName')
                >>= \case
                  Left e -> showError (Text.pack $ show e) appWin
                  Right json ->
                    case Aeson.eitherDecode' json of
                      Left e' -> showError (Text.pack e') appWin
                      Right json' ->
                        fromValue requestView json'
        else return ()
      Gtk.widgetDestroy dlg

showError :: (MonadIO m) => Text -> Gtk.Window -> m ()
showError text parentWin = do
  dlg <-
    new
      Gtk.MessageDialog
      [ #transientFor := parentWin,
        #modal := True,
        #text := text,
        #buttons := Gtk.ButtonsTypeClose,
        #title := "Error"
      ]
  void $ on dlg #response $ const $ Gtk.widgetDestroy dlg
  void $ Gtk.dialogRun dlg

sendRequest :: (MonadThrow m, MonadIO m) => RequestView -> m ()
sendRequest requestView@RequestView {..} = do
  setStatus resView "Request sent"
  req <- liftIO $ try @HTTP.HttpException $ createRequest requestView
  case req of
    Left e -> do
      let text = case e of
            HTTP.InvalidUrlException _ x -> Text.pack x
            _ -> Text.pack $ show e
      setStatus resView text
      showError text appWin
    Right req' -> do
      mgr <- HTTPS.newTlsManager
      liftIO $ void $ async $ do
        t1 <- Clock.getCurrentTime
        res <- try @HTTP.HttpException $ HTTP.httpLbs req' mgr
        t2 <- Clock.getCurrentTime
        let time = Clock.diffUTCTime t2 t1
        GLib.idleAdd GLib.PRIORITY_DEFAULT_IDLE $ do
          case res of
            Left e ->
              let text = case e of
                    HTTP.HttpExceptionRequest _ x -> Text.pack $ show x
                    _ -> Text.pack $ show e
               in do
                    setStatus resView "Failed"
                    showError text appWin
            Right res' ->
              fromResponse resView res' time
          return False

contentTypeChanged :: (MonadIO m) => RequestView -> m ()
contentTypeChanged RequestView {..} = do
  contentType <- Gtk.comboBoxTextGetActiveText (contentTypeCombo contentTypeView)
  lang <-
    case Text.strip <$> contentType of
      Nothing -> return Nothing
      Just "" -> return Nothing
      Just x -> GtkSource.languageManagerGuessLanguage lm Nothing (Just x)
  bufferMay <- liftIO $ Gtk.castTo GtkSource.Buffer =<< get bodyView #buffer
  case bufferMay of
    Just buffer -> do
      GtkSource.bufferSetLanguage buffer lang
    Nothing ->
      return ()
