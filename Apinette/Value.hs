{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}

module Apinette.Value
  ( IsValue (..),
  )
where

import Control.Monad.IO.Class (MonadIO (..))

class (MonadIO m) => IsValue m val where

  type Value val

  fromValue :: val -> (Value val) -> m ()

  toValue :: val -> m (Value val)
