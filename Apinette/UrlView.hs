{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

module Apinette.UrlView
  ( UrlView,
    sendButton,
  )
where

import qualified Apinette.Request as Val
import Apinette.Value (IsValue (..))
import Apinette.View (IsView (..))
import Control.Monad.IO.Class (MonadIO (..))
import Data.GI.Base
import qualified Data.Text.Encoding as Text
import qualified GI.Gtk as Gtk
import qualified Network.HTTP.Types as HTTP
import Network.HTTP.Types (StdMethod (..))

data UrlView
  = UrlView
      { box :: !Gtk.Box,
        methodCombo :: !Gtk.ComboBoxText,
        urlEntry :: !Gtk.Entry,
        sendButton :: !Gtk.Button
      }

instance (MonadIO m) => IsView m UrlView where

  type Props UrlView = ()

  create () = do
    box <-
      new
        Gtk.Box
        [ #orientation := Gtk.OrientationHorizontal,
          #spacing := 10
        ]
    methodCombo <- new Gtk.ComboBoxText []
    mapM_
      ( (\x -> Gtk.comboBoxTextAppend methodCombo (Just x) x)
          . Text.decodeUtf8
          . HTTP.renderStdMethod
      )
      [ GET,
        POST,
        PUT,
        DELETE
      ]
    set methodCombo [#active := 0]
    #add box methodCombo
    urlEntry <-
      new
        Gtk.Entry
        [ #hexpand := True,
          #placeholderText := "URL (ie. https://localhost)",
          #widthRequest := 400
        ]
    #add box urlEntry
    sendButton <- new Gtk.Button [#label := "Send"]
    #add box sendButton
    #showAll box
    return $ UrlView {..}

  widget = Gtk.toWidget . box

instance (MonadIO m) => IsValue m UrlView where

  type Value UrlView = (Maybe Val.Method, Maybe Val.Url)

  toValue UrlView {..} = do
    method <- get methodCombo #activeId
    url <- get urlEntry #text
    return $ (Val.Method <$> method, Just $ Val.Url url)

  fromValue UrlView {..} (methodMay, urlMay) = do
    case methodMay of
      Nothing ->
        set methodCombo [#active := 0]
      Just method ->
        set methodCombo [#activeId := Val.methodText method]
    set urlEntry [#text := maybe "" Val.urlText urlMay]
