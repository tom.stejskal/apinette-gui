{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

module Apinette.AuthView
  ( AuthView,
  )
where

import Apinette.BasicAuthView (BasicAuthView)
import Apinette.BearerAuthView (BearerAuthView)
import qualified Apinette.Request as Val
import Apinette.Value (IsValue (..))
import Apinette.View (IsView (..))
import Control.Monad (void)
import Control.Monad.IO.Class (MonadIO (..))
import Data.Bifunctor (first)
import Data.GI.Base
import Data.IORef (IORef, newIORef, readIORef, writeIORef)
import qualified Data.Text as Text
import qualified GI.Gtk as Gtk

data AuthView
  = AuthView
      { box :: !Gtk.Box,
        authTypeCombo :: !Gtk.ComboBoxText,
        detailView :: !(IORef AuthDetailView)
      }

data AuthDetailView
  = NoAuth
  | BasicAuth BasicAuthView
  | BearerAuth BearerAuthView

data AuthId
  = NoAuthId
  | BasicAuthId
  | BearerAuthId
  deriving (Read, Show)

instance (MonadIO m) => IsView m AuthView where

  type Props AuthView = ()

  create () = do
    box <-
      new
        Gtk.Box
        [ #orientation := Gtk.OrientationHorizontal,
          #spacing := 10
        ]
    authTypeCombo <- new Gtk.ComboBoxText []
    mapM_
      (uncurry (Gtk.comboBoxTextAppend authTypeCombo) . first (Just . Text.pack . show))
      [ (NoAuthId, "No Auth"),
        (BasicAuthId, "Basic Auth"),
        (BearerAuthId, "Bearer Auth")
      ]
    set authTypeCombo [#active := 0]
    #add box authTypeCombo
    detailView <- liftIO $ newIORef NoAuth
    let result = AuthView {..}
    void $ after authTypeCombo #changed $ authTypeChanged result
    return result
    where
      authTypeChanged authView@AuthView {..} =
        fmap (read . Text.unpack) <$> get authTypeCombo #activeId
          >>= createDetail authView
          >>= changeDetail authView

  widget = Gtk.toWidget . box

instance (MonadIO m) => IsValue m AuthView where

  type Value AuthView = Maybe Val.Auth

  toValue AuthView {..} =
    (liftIO $ readIORef detailView) >>= \case
      NoAuth -> return Nothing
      BasicAuth basicAuth -> Just . Val.Basic <$> toValue basicAuth
      BearerAuth bearerAuth -> Just . Val.Bearer <$> toValue bearerAuth

  fromValue authView@AuthView {..} = \case
    Nothing -> do
      set authTypeCombo [#activeId := Text.pack . show $ NoAuthId]
      createDetail authView Nothing >>= changeDetail authView
    Just (Val.Basic basic) -> do
      set authTypeCombo [#activeId := Text.pack . show $ BasicAuthId]
      basicAuth <- create ()
      detailView' <- createDetailFromBasicAuth authView basicAuth
      fromValue basicAuth basic
      changeDetail authView detailView'
    Just (Val.Bearer bearer) -> do
      set authTypeCombo [#activeId := Text.pack . show $ BearerAuthId]
      bearerAuth <- create ()
      detailView' <- createDetailFromBearerAuth authView bearerAuth
      fromValue bearerAuth bearer
      changeDetail authView detailView'

changeDetail :: (MonadIO m) => AuthView -> AuthDetailView -> m ()
changeDetail AuthView {..} detailView' = do
  liftIO $ readIORef detailView >>= destroyDetail
  liftIO $ writeIORef detailView detailView'

createDetail :: (MonadIO m) => AuthView -> Maybe AuthId -> m (AuthDetailView)
createDetail AuthView {..} = \case
  Just BasicAuthId -> do
    basicAuth <- create ()
    #add box =<< widget basicAuth
    return $ BasicAuth basicAuth
  Just BearerAuthId -> do
    bearerAuth <- create ()
    #add box =<< widget bearerAuth
    return $ BearerAuth bearerAuth
  _ -> return NoAuth

createDetailFromBasicAuth :: (MonadIO m) => AuthView -> BasicAuthView -> m (AuthDetailView)
createDetailFromBasicAuth AuthView {..} basicAuth = do
  #add box =<< widget basicAuth
  return $ BasicAuth basicAuth

createDetailFromBearerAuth :: (MonadIO m) => AuthView -> BearerAuthView -> m (AuthDetailView)
createDetailFromBearerAuth AuthView {..} bearerAuth = do
  #add box =<< widget bearerAuth
  return $ BearerAuth bearerAuth

destroyDetail :: (MonadIO m) => AuthDetailView -> m ()
destroyDetail = \case
  NoAuth -> return ()
  BasicAuth basicAuth ->
    destroy basicAuth
  BearerAuth bearerAuth ->
    destroy bearerAuth
