{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}

module Apinette.View
  ( IsView (..),
  )
where

import Control.Monad ((<=<))
import Control.Monad.IO.Class (MonadIO (..))
import qualified GI.Gtk as Gtk

class (MonadIO m) => IsView m view where

  type Props view

  create :: Props view -> m view

  destroy :: view -> m ()
  destroy = Gtk.widgetDestroy <=< widget

  widget :: view -> m Gtk.Widget
