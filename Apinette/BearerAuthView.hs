{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

module Apinette.BearerAuthView
  ( BearerAuthView,
  )
where

import qualified Apinette.Request as Val
import Apinette.Value (IsValue (..))
import Apinette.View (IsView (..))
import Control.Monad.IO.Class (MonadIO (..))
import Data.GI.Base
import qualified GI.Gtk as Gtk

data BearerAuthView
  = BearerAuthView
      { box :: !Gtk.Box,
        tokenEntry :: !Gtk.Entry
      }

instance (MonadIO m) => IsView m BearerAuthView where

  type Props BearerAuthView = ()

  create () = do
    box <-
      new
        Gtk.Box
        [ #orientation := Gtk.OrientationHorizontal,
          #spacing := 10
        ]
    tokenEntry <- new Gtk.Entry [#placeholderText := "Token"]
    #add box tokenEntry
    #showAll box
    return $ BearerAuthView {..}

  widget = Gtk.toWidget . box

instance (MonadIO m) => IsValue m BearerAuthView where

  type Value BearerAuthView = Val.BearerAuth

  toValue BearerAuthView {..} = do
    token <- get tokenEntry #text
    return $ Val.BearerAuth token

  fromValue BearerAuthView {..} Val.BearerAuth {..} =
    set tokenEntry [#text := token]
