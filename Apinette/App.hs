{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}

module Apinette.App
  ( run,
  )
where

import Apinette.RequestView
  ( RequestView,
    open,
    save,
    saveAs,
    sendRequest,
  )
import Apinette.View (IsView (..))
import Control.Monad (void)
import Control.Monad.Catch (MonadThrow (..))
import Control.Monad.IO.Class (MonadIO (..))
import Data.GI.Base
import Data.Text (Text)
import qualified GI.Gdk as Gdk
import qualified GI.Gtk as Gtk

run :: (MonadThrow m, MonadIO m) => Text -> m ()
run appName = do
  void $ Gtk.init Nothing
  appWin <- new Gtk.Window [#title := appName]
  --Gtk.widgetSetSizeRequest appWin 800 600
  void $ on appWin #destroy Gtk.mainQuit
  accelGroup <- new Gtk.AccelGroup []
  Gtk.windowAddAccelGroup appWin accelGroup
  mapM_
    (\(p, k, m) -> Gtk.accelMapAddEntry p k m)
    [ ("<Request>/File/Open", Gdk.KEY_O, [Gdk.ModifierTypeControlMask]),
      ("<Request>/File/Save", Gdk.KEY_S, [Gdk.ModifierTypeControlMask]),
      ("<Request>/File/Quit", Gdk.KEY_Q, [Gdk.ModifierTypeControlMask]),
      ("<Request>/Action/Send", Gdk.KEY_Return, [Gdk.ModifierTypeControlMask])
    ]
  box <- new Gtk.Box [#orientation := Gtk.OrientationVertical]
  requestView <- create @_ @RequestView (appWin, appName)
  menuBar <- createMenuBar accelGroup requestView
  #add box menuBar
  #add box =<< widget requestView
  #add appWin box
  #showAll appWin
  Gtk.main

createMenuBar :: (MonadIO m) => Gtk.AccelGroup -> RequestView -> m Gtk.MenuBar
createMenuBar accelGroup requestView = do
  result <- new Gtk.MenuBar []
  mapM_
    (#add result =<<)
    [ createFileMenu accelGroup requestView,
      createActionMenu accelGroup requestView
    ]
  return result

createFileMenu :: (MonadIO m) => Gtk.AccelGroup -> RequestView -> m Gtk.MenuItem
createFileMenu accelGroup requestView = do
  subMenu <- new Gtk.Menu [#accelGroup := accelGroup]
  openItem <- new Gtk.MenuItem [#label := "Open", #accelPath := "<Request>/File/Open"]
  void $ on openItem #activate $ open requestView
  saveItem <- new Gtk.MenuItem [#label := "Save", #accelPath := "<Request>/File/Save"]
  void $ on saveItem #activate $ save requestView
  saveAsItem <- new Gtk.MenuItem [#label := "Save as"]
  void $ on saveAsItem #activate $ saveAs requestView
  quitItem <- new Gtk.MenuItem [#label := "Quit", #accelPath := "<Request>/File/Quit"]
  void $ on quitItem #activate Gtk.mainQuit
  mapM_ (#add subMenu) [openItem, saveItem, saveAsItem, quitItem]
  new
    Gtk.MenuItem
    [ #label := "File",
      #submenu := subMenu
    ]

createActionMenu :: (MonadIO m) => Gtk.AccelGroup -> RequestView -> m Gtk.MenuItem
createActionMenu accelGroup requestView = do
  subMenu <- new Gtk.Menu [#accelGroup := accelGroup]
  openItem <- new Gtk.MenuItem [#label := "Send", #accelPath := "<Request>/Action/Send"]
  void $ on openItem #activate $ sendRequest requestView
  mapM_ (#add subMenu) [openItem]
  new
    Gtk.MenuItem
    [ #label := "Action",
      #submenu := subMenu
    ]
