{-# LANGUAGE MultiParamTypeClasses #-}

module Apinette.Response
  ( FromResponse (..),
  )
where

import Control.Monad.IO.Class (MonadIO (..))
import Data.Time.Clock (NominalDiffTime)
import qualified Network.HTTP.Client as HTTP

class (MonadIO m) => FromResponse m body res where
  fromResponse :: res -> HTTP.Response body -> NominalDiffTime -> m ()
