{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

module Apinette.BasicAuthView
  ( BasicAuthView,
  )
where

import qualified Apinette.Request as Val
import Apinette.Value (IsValue (..))
import Apinette.View (IsView (..))
import Control.Monad.IO.Class (MonadIO (..))
import Data.GI.Base
import qualified GI.Gtk as Gtk

data BasicAuthView
  = BasicAuthView
      { box :: !Gtk.Box,
        userEntry :: !Gtk.Entry,
        passEntry :: !Gtk.Entry
      }

instance (MonadIO m) => IsView m BasicAuthView where

  type Props BasicAuthView = ()

  create () = do
    box <-
      new
        Gtk.Box
        [ #orientation := Gtk.OrientationHorizontal,
          #spacing := 10
        ]
    userEntry <- new Gtk.Entry [#placeholderText := "User"]
    #add box userEntry
    passEntry <- new Gtk.Entry [#placeholderText := "Password"]
    set passEntry [#visibility := False]
    #add box passEntry
    #showAll box
    return $ BasicAuthView {..}

  widget = Gtk.toWidget . box

instance (MonadIO m) => IsValue m BasicAuthView where

  type Value BasicAuthView = Val.BasicAuth

  toValue BasicAuthView {..} = do
    user <- get userEntry #text
    pass <- get passEntry #text
    return $ Val.BasicAuth user pass

  fromValue BasicAuthView {..} Val.BasicAuth {..} = do
    set userEntry [#text := user]
    set passEntry [#text := pass]
