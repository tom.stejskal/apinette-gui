{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified Apinette.App as App

main :: IO ()
main = App.run "Apinette"
